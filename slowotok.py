#!/usr/bin/env python
#coding: utf8 
import os
import ast
import sys
from pymouse import PyMouse
import time
import urllib2

m = PyMouse()

words_written = []

mouse = False
mouse_points = []
two_points_list = []

if len(sys.argv) == 3:
	mouse = True
	two_points_list = ast.literal_eval(sys.argv[2])

if two_points_list:
	height_diff = two_points_list[1][1]-two_points_list[0][1]
	width_diff = two_points_list[1][0]-two_points_list[0][0]

	height_dist = height_diff/4
	width_dist = width_diff/4

	height_middle = height_dist/2
	width_middle = width_dist/2


	for y in range(0,4):
		_list = []
		for x in range(0,4):
			_x = two_points_list[0][0]+x*width_dist+width_middle
			_y = two_points_list[0][1]+y*height_dist+height_middle
			_list.append((_x,_y))
		mouse_points.append(_list)

lista_slow = []

#with open ("sjp.txt", "r") as myfile:
#	data=myfile.read()
#	lista_slow = data.split("\n")

#lista_ok = []
#for slowo in lista_slow:
#	slowo = slowo.replace('\r','')
#	lista_ok.append(slowo)

#lista_slow = lista_ok
#file_slowa = open("slowa.txt")
#for slowo in file_slowa:
#	lista_slow.append(slowo)

#print lista_slow

def is_polish(x1,x2):
	x = x1+x2
	if x == "ą": return u"ą"
	if x == "ć": return u"ć"
	if x == "ł": return u"ł"
	if x == "ó": return u"ó"
	if x == "ż": return u"ż"
	if x == "ź": return u"ź"
	if x == "ę": return u"ę"
	if x == "ś": return u"ś"
	if x == "ń": return u"ń"

def is_char(x):
	#list = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','R','S','T','U','W','X','Y','Z']
	list = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','r','s','t','u','w','x','y','z','q']
	if x in list: return x

def do_mouse_move(moves):
	x = 0
	y = 0
	for mv in moves:
		time.sleep(0.1)
		x = mouse_points[mv[0]][mv[1]][0]
		y = mouse_points[mv[0]][mv[1]][1]
		m.press(x,y,1)
	m.release(100,100,1)

words_not_exists = []
letters = sys.argv[1]
letters = letters+"d"
#letters = letters.upper()
list_letters = []
for i in range(0, len(letters)):
	if i<len(letters)-1:
		pl = is_polish(letters[i],letters[i+1])
		znak = is_char(letters[i])
		if pl:
			list_letters.append(pl)
		elif znak:
			list_letters.append(znak)
#	list_letters.append(letter)

letters=u""

for letter in list_letters:
	letters = letters+letter

#new_list_letters=list_letters[:]
#list_letters=[]
#for letter in new_list_letters:
#	list_letters.append(letter.encode("utf-8"))

words = ["aeim"]
found_words = []

i = 0
full_list = []
small_list = []
for letter in list_letters:
	small_list.append(letter)
	i+=1
	if i==4:
		i = 0
		full_list.append(small_list)
		small_list = []

print full_list
#################GET WEBSERVICE
response = urllib2.urlopen((u"http://karpdami.linuxpl.info/dictionary/?letters="+letters[:16]).encode("UTF-8"))
words_dict = ast.literal_eval(response.read())
for word in words_dict:                                                     
	lista_slow.append(word['word'].replace("\u015b",u"ś").replace("\u0107",u"ć").replace("\u00f3",u"ó").replace("\u017c",u"ż").replace("\u017a",u"ź").replace("\u0142",u"ł").replace("\u0109",u"ę").replace("\u0105",u"ą").replace("\u0144",u"ń"))
#.replace("\u015",u"ś")

#print lista_slow

print "5"
time.sleep(1)
print "4"
time.sleep(1)
print "3"
time.sleep(1)
print "2"
time.sleep(1)
print "1"
time.sleep(1)
#################
tuples=[]

def word_exists(wordlist, word_fragment):
	ret = any(w.startswith(word_fragment) for w in wordlist)
	return ret

def where_can_move(x, y, done_moves):
	list_to_move = []
	for pos_x in range(-1,2):
		for pos_y in range(-1,2):
			new_x = x+pos_x
			new_y = y +pos_y
			if pos_x != 0 or pos_y != 0:
				if new_x>=0 and new_x<=3:
					if new_y>=0 and new_y<=3:
						if (new_x, new_y) not in done_moves:
							list_to_move.append((new_x,new_y))
	return list_to_move

def do_move(x, y, done_moves):
	moves = where_can_move(x,y,done_moves)
	word = ""
	not_exist = False
	for move in done_moves:
		word = word + full_list[move[0]][move[1]]
	if not word_exists(lista_slow,word):
		not_exist = True
		#print "a: "+word
	if not not_exist:
		if word in lista_slow:
			if len(word)>2:
				print word
				if word not in words_written:
					do_mouse_move(done_moves)
					words_written.append(word)
			found_words.append(word)
		for move in moves:
			copy = done_moves[:]
			copy.append((move[0],move[1]))
			do_move(move[0],move[1],copy)

for x in range(0,4):
	for y in range(0,4):
		pox_x = x
		pos_y = y
		moves_did = [(x,y)]
		do_move(x, y, moves_did[:])

print found_words
#if slowo in lista: print True








