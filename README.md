## [Słowotok](http://www.slowotok.pl)

Słowotok to gra w której musimy znaleźć wszystkie słowa występujące w polskim słowniku w formie podstawowej.

Projekt wykorzystuje biblioteki pythonowe:

* PyMouse

* os

* ast

* sys

* time

* urllib2

By je zainstalować w systemie Linux należy wpisać
* sudo pip install [nazwa]

## Jak uruchomić

Należy pobrać repozytorium (po prawej download), albo je sklonować do folderu 

* git clone https://belthezer@bitbucket.org/belthezer/slowotok.git

Za pomocą konsoli/termnala należy wejść do folderu z plikami i wpisać komendę

* python mysz.py

Należy wtedy (mając ciągle aktywny terminal) najechać mysza na lewy górny róg macierzy z literkami.
Wcisnąć enter (w terminalu pojawi się informacja o pobraniu koordynatów).
Najechać myszą na prawy dolny róg macierzy z literkami
Wcisnąć enter.

Przykładowy wynik operacji to:

* [(109, 250), (453, 613)]

Potem należy wpisać w terminalu:

* python slowotok.py [ciąg liter] "[koordynaty]"

Czyli przykładowo

* python slowotok.py duazhdnejufywios "[(109, 250), (453, 613)]"

Program zacznie wyszukiwać pasujących słów i myszą będzie je automatycznie zaznaczał na planszy.


## Licencja

Slowotok jest darmowy w użyciu i w wolnej dystrybucji.
